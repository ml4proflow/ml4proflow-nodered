def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(u'--algorithm',action='store')
    parser.add_argument(u'--extra-args', action='store', nargs='*')
    parser.add_argument(u'--debug', action='store_true')
    args = parser.parse_args()

    import sys
    import os
    import importlib
    import pandas
    import json
    from ml4proflow.module_finder import find_by_name
    from ml4proflow.modules import DataFlowManager

    module_args = {"channels_push":["to_node_red"],
                   "channels_pull":["from_node_red"]} # default module config
    for arg in args.extra_args:
        if len(arg)>0:
            k,v = arg.split(":",1)
            sys.stderr.writelines("key '%s': value '%s'" % (k,v))
            module_args[k] = v

    sys.stderr.writelines("args %s" % args)
    sys.stderr.flush()

    _class = find_by_name(args.algorithm) # find class
    dfm = DataFlowManager()
    dfm.create_channel('from_node_red')
    instance = _class(dfm, module_args)

    sys.stderr.writelines("loaded %s" % _class)
    sys.stderr.flush()


    def update_status(status):
        sys.stderr.writelines("%s: Status: %s" % (args.algorithm, status))
        sys.stderr.flush()
        pass

    instance.update_status = update_status # override status update method

    def send_results(channel, data):
        to_send = data.to_json()
        sys.stdout.write(to_send)
        sys.stderr.write("%d bytes\n" % len(str(to_send)))
        sys.stderr.flush()
        sys.stdout.flush()

    instance._push_data = send_results #override return path

    sys.stderr.writelines("waiting for input")
    sys.stderr.flush()
    #process input
    for l in sys.stdin: #Todo: Handle \n in user data
        sys.stderr.writelines("got input: "+args.algorithm)
        sys.stderr.flush()
        sys.stdout.flush()
        if args.debug:
            sys.stderr.writelines("input: %s" % repr(l))
            sys.stderr.flush()
        input_data = pandas.read_json(l)
        instance.on_new_data("nodered", None, input_data)
        sys.stderr.writelines("done processing")
        sys.stderr.flush()
