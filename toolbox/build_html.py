import dominate
from dominate.tags import *
from matplotlib.pyplot import cla

_script = script(type="text/html")
template = _script.add(div())

template['class'] = "form-row"
with template:
    attr(data-template-name=="toolbox")
with template:
    label()

print(_script)


""" finder = div(cls='form-row')
#finder[label(fr = 'node-input-name', cls='fa fa-tag')]
with finder.div:
    label(fr = 'node-input-name', cls='fa fa-tag') 
print(finder)
 """