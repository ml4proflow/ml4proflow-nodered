module.exports = function(RED) {
    function toolbox(config) {
		const { spawn } = require('child_process');
		var iconv  = require('iconv-lite');
		RED.nodes.createNode(this,config);
		this.algorithm = config.algorithm;
		this.extraargs = config.extraargs;
		var node = this;
		if (this.algorithm == 'Toolboxverbinder') {
			const net = require('net');
			const client = net.createConnection({ port: 12345 }, () => {
			  // 'connect' listener.
			  console.log('connected to server!');
			});
			client.on('error', function (err) {
			console.log(err);
			});
			node.client = client;
			client.on('data', (data) => {
			  console.log(data.toString());
			  client.end();
			});
			client.on('end', () => {
			  console.log('disconnected from server');
			});
			node.on('input', function(msg) {
				node.client.write(JSON.stringify(msg['payload']).toString('utf8'));
				node.client.write('\n');
				console.log("done sending");
			});
		} else {
			node.python = spawn('python', ['./node-red/toolbox/launcher.py ', '--algorithm',node.algorithm, '--extra-args',node.extraargs], {maxBuffer: 1024*1024*32,encoding:'uft8',stdio: ['pipe', 'pipe', 'pipe']})
			node.python.stdout.on('data', function(msg) {
				console.log('stdout:',msg.toString('utf8'));
				node.send([{payload:JSON.parse(msg.toString('utf8').replace("'",'"'))}]);
			});
			node.python.stderr.on('data', function(msg) {
				console.log('stderr:',msg.toString('utf8'));
			});
			node.python.on('close', function(exitCode) {
				console.log('Python closed: code:',exitCode);
			});
			
			node.on('input', function(msg) {
				node.python.stdin.write(JSON.stringify(msg['payload']).toString('utf8'));
				node.python.stdin.write('\n');
				console.log(node)
			});
		}
	}
	console.log('run export function');
	RED.nodes.registerType("toolbox",toolbox);
}