import argparse
parser = argparse.ArgumentParser()
parser.add_argument(u'--algorithm',action='store')
parser.add_argument(u'--extra-args', action='store', nargs='*')
args = parser.parse_args()

import sys
import os
import importlib
import pandas
import json
sys.stderr.writelines("args %s" % args)
sys.stderr.flush()
module_path = "%s/../../" % os.path.dirname(os.path.abspath(__file__))
sys.path.append(module_path)
module = importlib.import_module('toolbox.io.basyx.wrapper.%s_wrapper' % args.algorithm)
_classname = ''
for name in args.algorithm.split('_'):
    _classname += name.capitalize()
_classname += 'Wrapper'
sys.stderr.writelines("loaded %s" % _classname)
sys.stderr.flush()

_class = getattr(module,_classname[0].upper()+_classname[1:]) #create instance from class

instance = _class("")

for arg in args.extra_args:
    if len(arg)>0:
        k,v = arg.split(":",1)
        sys.stderr.writelines("key '%s': value '%s'" % (k,v))
        setattr(instance,k,v)

def update_status(status):
    sys.stderr.writelines("%s: Status: %s" % (args.algorithm, status))
    sys.stderr.flush()
    pass

instance.update_status = update_status # override status update method

def send_results(data):
    to_send = data.to_json()
    sys.stdout.write(to_send)
    sys.stderr.write("%d bytes\n" % len(str(to_send)))
    sys.stderr.flush()
    sys.stdout.flush()

instance._push_data = send_results #override return path

sys.stderr.writelines("waiting for input")
sys.stderr.flush()
#process input
for l in sys.stdin: #Todo: Handle \n in user data
    sys.stderr.writelines("got input: "+args.algorithm)
    sys.stderr.flush()
    sys.stdout.flush()
    input_data = pandas.read_json(l)
    instance.on_new_data(input_data)
    sys.stderr.writelines("done processing")
    sys.stderr.flush()
