from setuptools import setup, find_namespace_packages

long_description = ""

name = "ml4proflow-nodered"
version = "0.0.1"

cmdclass = {}

try:
    from sphinx.setup_command import BuildDoc
    cmdclass['build_sphinx'] = BuildDoc
except ImportError:
    print('WARNING: Sphinx not available, not building docs')

setup(
    name=name,
    version=version,
    author="Christian Klarhorst",
    author_email="cklarhor@techfak.uni-bielefeld.de",
    description="Framework",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-nodered",
    project_urls={
        "Main framework": "https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow",
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        'Intended Audience :: Developers',
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=find_namespace_packages(where="src"),
    namespace_packages=['ml4proflow_mods'],

    entry_points={
        'console_scripts': ['nodered_launcher=ml4proflow_mods.nodered.launcher:main', ],
    },
    cmdclass=cmdclass,
    python_requires=">=3.6",  # todo
    install_requires=[
        "ml4proflow",
        "pandas",
    ],
    extras_require={
        "tests": ["pytest", 
                  "pandas-stubs",
                  "pytest-html",
                  "pytest-cov",
                  "flake8",
                  "mypy",
                  "jinja2==3.0.3"],
        "docs": ["sphinx", "sphinx-rtd-theme", "recommonmark"],
    },
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', version),
            'source_dir': ('setup.py', 'docs/source/'),
            'build_dir': ('setup.py', 'docs/build/')
        }
    },
)
